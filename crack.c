#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "md5.h"

const int PASS_LEN=50;        // Maximum any password can be
const int HASH_LEN=33;        // Length of MD5 hash strings


// Stucture to hold both a plaintext password and a hash.
struct entry 
{
    // TODO: FILL THIS IN
    char password[PASS_LEN];
    char hash[HASH_LEN];
};



// TODO
// Read in the dictionary file and return an array of structs.
// Each entry should contain both the hash and the dictionary
// word.
struct entry *read_dictionary(char *filename, int *size)
{
    struct entry *dictionary = malloc(50 * sizeof(struct entry));
    
    FILE *fp = fopen(filename, "r");
    if (!fp)
    {
        printf("Can't open %s for reading\n", filename);
        exit(2);
    }
    
    char line[1000];
    *size = 50;
    int count = 0;
    //fprintf(stderr, "Count:%d Size:%d\n", count, *size);
    
    while (fgets(line, 1000, fp) != NULL)
    {
        //fprintf(stderr, "Count:%d Size:%d\n", count, *size);
        if (*size == count)
        {
            *size += 50;
            dictionary = realloc(dictionary, *size * sizeof(struct entry));
        }
        
        char *dictionaryMalloc = malloc(strlen(line) * sizeof(char) + 1);
        
        strcpy(dictionaryMalloc, line);
        strcpy(dictionary[count].password, dictionaryMalloc);
        strcpy(dictionary[count].hash, md5(line, strlen(line) - 1));
        
        //printf("%s\n", dictionary[count].password);
        count++;
    }
    
    *size = count;
    return dictionary;
}

int cmpfunc (const void * a, const void * b)
{
    struct entry *aa = (struct entry*)a;
    struct entry *bb = (struct entry*)b;
    
    return ( strcmp(aa->hash, bb->hash) );
}

int searchfunc (const void * a, const void * b)
{
    char *aa = (char *)a;
    struct entry *bb = (struct entry*)b;
    
    return ( strcmp(aa, bb->hash) );
}

int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // TODO: Read the dictionary file into an array of entry structures
    int dlen = 0;
    struct entry *dict = read_dictionary(argv[2], &dlen);
    
    // TODO: Sort the hashed dictionary using qsort.
    // You will need to provide a comparison function.
    qsort(dict, dlen, sizeof(struct entry), cmpfunc);
    for (int i = 0; i < dlen; i++)
    {
        //printf("Password:%s Hash:%s\n", dict[i].password, dict[i].hash);
    }
    //printf("\n");
    
    // TODO
    // Open the hash file for reading.
    FILE *fp = fopen(argv[1], "r");
    if (!fp)
    {
        printf("Can't open %s for reading\n", argv[1]);
        exit(2);
    }

    // TODO
    // For each hash, search for it in the dictionary using
    // binary search.
    // If you find it, get the corresponding plaintext dictionary word.
    // Print out both the hash and word.
    // Need only one loop. (Yay!)
    char line[1000];

    while (fgets(line, 1000, fp) != NULL)
    {
        line[strlen(line) - 1] = '\0';
        struct entry *temp = bsearch(line, dict, dlen, sizeof(struct entry), searchfunc);
        if (temp)
        {
            printf("Hash:%s Plaintest:%s", temp->hash, temp->password);    
        }
    }
}
